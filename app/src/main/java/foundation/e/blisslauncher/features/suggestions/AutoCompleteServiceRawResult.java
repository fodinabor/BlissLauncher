package foundation.e.blisslauncher.features.suggestions;

public class AutoCompleteServiceRawResult {

    private String phrase;

    public AutoCompleteServiceRawResult(String phrase){
        this.phrase = phrase;
    }

    public String getPhrase() {
        return phrase;
    }
}
