package foundation.e.blisslauncher.features.calendar;

import android.annotation.SuppressLint;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.provider.CalendarContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import foundation.e.blisslauncher.R;
import foundation.e.blisslauncher.core.Utilities;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class EventlistBuilder {

    private static final String TAG = "EventlistBuilder";

    /**
     * This method is used to build the small, horizontal forecasts panel
     *
     * @param context    Context to be used
     * @param smallPanel a horizontal linearlayout that will contain the forecasts
     * @param events     the Weather info object that contains the forecast data
     */
    @SuppressLint({"InflateParams", "DefaultLocale"})
    public static void buildSmallPanel(Context context, LinearLayout smallPanel, List<CalendarEvent> events) {
        if (smallPanel == null) {
            Log.d(TAG, "Invalid view passed");
            return;
        }
        try {
            // Get things ready
            LayoutInflater inflater
                    = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            smallPanel.removeAllViews();

            Calendar date = Calendar.getInstance();
            SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
            String currentDate = format.format(date.getTime());

            int day_id = 0;

            assert inflater != null;
            // Load the calendar view
            // create todays day layout and set the day info
            View dayItem = inflater.inflate(R.layout.item_calendar_day, null);
            dayItem.setTag(String.format("cal_day%d", day_id++));

            { // don't reuse variables later..
                //setup for opening the calendar for the tapped on date
                View dateIcon = dayItem.findViewById(R.id.calendar_day_date_icon);
                Uri.Builder builder = CalendarContract.CONTENT_URI.buildUpon();
                builder.appendPath("time");
                ContentUris.appendId(builder, date.getTimeInMillis());
                Intent openCalendarIntent = new Intent(Intent.ACTION_VIEW)
                        .setData(builder.build());
                dateIcon.setOnClickListener(v -> context.startActivity(openCalendarIntent));
            }

            LinearLayout eventsList = dayItem.findViewById(R.id.calendar_day_event_list);
            TextView dateView = dayItem.findViewById(R.id.calendar_day_date_textview);
            TextView monthView = dayItem.findViewById(R.id.calendar_day_month_textview);

            dateView.setText(String.format(Locale.getDefault(), "%d", date.get(Calendar.DAY_OF_MONTH)));
            monthView.setText(Utilities.convertMonthToString(date.get(Calendar.MONTH)));

            // Iterate through the Events
            for (int count = 0; count < events.size(); count++) {
                CalendarEvent event = events.get(count);
                String eventDate = format.format(event.getStartMillis());
                if (!eventDate.equals(currentDate) && event.getStartMillis() >= date.getTimeInMillis()) {
                    currentDate = eventDate;
                    if (smallPanel.findViewWithTag(dayItem.getTag()) == null && dayItem.findViewWithTag("event_item") != null) {
                        smallPanel.addView(dayItem);
                    }

                    // create new day layout and set the day info
                    dayItem = inflater.inflate(R.layout.item_calendar_day, null);
                    dayItem.setTag(String.format("cal_day%d", day_id++));

                    {
                        //setup for opening the calendar for the tapped on date
                        View dateIcon = dayItem.findViewById(R.id.calendar_day_date_icon);
                        Uri.Builder builder = CalendarContract.CONTENT_URI.buildUpon();
                        builder.appendPath("time");
                        ContentUris.appendId(builder, event.getStartMillis());
                        Intent openCalendarIntent = new Intent(Intent.ACTION_VIEW)
                                .setData(builder.build());
                        dateIcon.setOnClickListener(v -> context.startActivity(openCalendarIntent));
                    }

                    eventsList = dayItem.findViewById(R.id.calendar_day_event_list);
                    dateView = dayItem.findViewById(R.id.calendar_day_date_textview);
                    monthView = dayItem.findViewById(R.id.calendar_day_month_textview);

                    dateView.setText(String.format(Locale.getDefault(), "%d", event.getStartDate().get(Calendar.DAY_OF_MONTH)));
                    monthView.setText(Utilities.convertMonthToString(event.getStartDate().get(Calendar.MONTH)));
                }

                View eventItem = inflater.inflate(R.layout.item_calendar_event, null);
                eventItem.setTag("event_item");

                TextView title = eventItem.findViewById(R.id.calendar_event_title);
                title.setText(event.getTitle());

                TextView location = eventItem.findViewById(R.id.calendar_event_location);
                if (event.getLocation() == null || event.getLocation().isEmpty()) {
                    location.setVisibility(View.GONE);
                } else {
                    location.setText(event.getLocation());
                }

                TextView time = eventItem.findViewById(R.id.calendar_event_time);
                time.setText(event.getTimeFormatted());

                ImageView calendarColor = eventItem.findViewById(R.id.calendar_color);
                calendarColor.setBackgroundColor(event.getCalendarColor());

                Uri uri = ContentUris.withAppendedId(CalendarContract.Events.CONTENT_URI, event.getEventId());
                eventItem.setOnClickListener(v -> context.startActivity(new Intent(Intent.ACTION_VIEW).setData(uri)));
                eventsList.addView(eventItem);
            }
            if (smallPanel.findViewById(dayItem.getId()) == null) {
                smallPanel.addView(dayItem);
            }
        } catch (Resources.NotFoundException nfe) {
            nfe.printStackTrace();
        }
    }
}
