package foundation.e.blisslauncher.features.calendar;

import android.Manifest;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.IBinder;
import android.provider.CalendarContract.Instances;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import foundation.e.blisslauncher.core.utils.Constants;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class CalendarUpdateService extends Service {
    private static final String TAG = "CalendarUpdateService";
    private static final boolean D = Constants.DEBUG;

    public static final String ACTION_FORCE_UPDATE =
            "org.indin.blisslauncher.action.FORCE_CALENDAR_UPDATE";
    // Broadcast action for end of update
    public static final String ACTION_UPDATE_FINISHED =
            "org.indin.blisslauncher.action.CALENDAR_UPDATE_FINISHED";


    public static final String[] INSTANCE_PROJECTION = new String[]{
            Instances.EVENT_ID,      // 0
            Instances.BEGIN,         // 1
            Instances.END,           // 2
            Instances.TITLE,         // 3
            Instances.EVENT_LOCATION,// 4
            Instances.ALL_DAY,       // 5
            Instances.DISPLAY_COLOR  // 6
    };

    // The indices for the projection array above.
    private static final int PROJECTION_ID_INDEX = 0;
    private static final int PROJECTION_BEGIN_INDEX = 1;
    private static final int PROJECTION_END_INDEX = 2;
    private static final int PROJECTION_TITLE_INDEX = 3;
    private static final int PROJECTION_LOCATION_INDEX = 4;
    private static final int PROJECTION_ALL_DAY_INDEX = 5;
    private static final int PROJECTION_COLOR_INDEX = 6;

    private CalendarChangeReceiver mReceiver;

    @Override
    public void onCreate() {
        Log.d(TAG, "onCreate");

        IntentFilter filter = new IntentFilter();
        filter.addAction("android.intent.action.PROVIDER_CHANGED");
        filter.addDataScheme("content");
        filter.addDataAuthority("com.android.calendar", null);
        filter.setPriority(999);
        mReceiver = new CalendarChangeReceiver();
        registerReceiver(mReceiver, filter);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (D) Log.v(TAG, "Got intent " + intent);

        if (!hasCalendarPermission(this)) {
            Log.d(TAG, "Service started, but noc calendar permissions ... stopping");
            stopSelf();
            return START_NOT_STICKY;
        }

        if (ACTION_FORCE_UPDATE.equals(intent.getAction())) {
            mReceiver.updateEvents();
        }

        return START_REDELIVER_INTENT;
    }

    public class CalendarChangeReceiver extends BroadcastReceiver {
        // constructor
        public CalendarChangeReceiver() {

        }

        @Override
        public void onReceive(Context context, Intent intent) {
            updateEvents();
        }

        public void updateEvents() {
            List<CalendarEvent> events = getUpcomingEvents(7);
            foundation.e.blisslauncher.core.Preferences.setCachedCalendarEvents(getApplicationContext(), events);
            Intent updateIntent = new Intent(CalendarUpdateService.ACTION_UPDATE_FINISHED);
            LocalBroadcastManager.getInstance(CalendarUpdateService.this).sendBroadcast(updateIntent);
        }

        private List<CalendarEvent> getUpcomingEvents(int nextDays) {
            List<CalendarEvent> retEvents = new ArrayList<>();

            // Load events from now to the next 7 days
            Calendar beginTime = Calendar.getInstance();
            long startMillis = beginTime.getTimeInMillis();
            Calendar endTime = Calendar.getInstance();
            endTime.add(Calendar.DATE, nextDays);
            long endMillis = endTime.getTimeInMillis();
            long offset = beginTime.getTimeZone().getOffset(startMillis);

            Cursor cur = null;
            ContentResolver cr = getContentResolver();

            String selection = Instances.VISIBLE + " <> 0";
            String order = Instances.BEGIN + " ASC";

            // Construct the query with the desired date range.
            Uri.Builder builder = Instances.CONTENT_URI.buildUpon();
            ContentUris.appendId(builder, startMillis);
            ContentUris.appendId(builder, endMillis);

            // Submit the query
            cur = cr.query(builder.build(),
                    INSTANCE_PROJECTION,
                    selection,
                    null,
                    order);

            if (cur != null && cur.getCount() > 0) {
                while (cur.moveToNext()) {
                    int eventId = 0;
                    String title = null;
                    String location = null;
                    long beginVal = 0;
                    long endVal = 0;
                    boolean allDay = false;
                    int color = 0;

                    // Get the field values
                    eventId = cur.getInt(PROJECTION_ID_INDEX);
                    beginVal = cur.getLong(PROJECTION_BEGIN_INDEX);
                    endVal = cur.getLong(PROJECTION_END_INDEX);
                    title = cur.getString(PROJECTION_TITLE_INDEX);
                    location = cur.getString(PROJECTION_LOCATION_INDEX);
                    allDay = cur.getInt(PROJECTION_ALL_DAY_INDEX) != 0;
                    color = cur.getInt(PROJECTION_COLOR_INDEX);

                    if (allDay) { // all day events are saved in UTC not local timezone, we have to correct the values
                        beginVal -= offset;
                        endVal -= offset;
                        if (endVal < startMillis)
                            continue;
                    }

                    CalendarEvent ev = new CalendarEvent(getApplicationContext(), eventId, title, allDay, beginVal, endVal, location, color);
                    retEvents.add(ev);
                }
                cur.close();
            }

            return retEvents;
        }
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy");
        unregisterReceiver(mReceiver);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public static boolean hasCalendarPermission(Context context) {
        return context.checkSelfPermission(Manifest.permission.READ_CALENDAR)
                == PackageManager.PERMISSION_GRANTED;
    }
}
