package foundation.e.blisslauncher.features.calendar;

import android.content.Context;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Locale;

public class CalendarEvent {
    private transient Context context;
    private final int eventId;
    private final String title;
    private final boolean allDay;
    private final Calendar startDate;
    private final Calendar endDate;
    private final long startMillis;
    private final long endMillis;
    private final String location;
    private final int calendarColor;

    public void setContext(Context context) {
        this.context = context;
    }

    public int getEventId() {
        return eventId;
    }

    public String getTitle() {
        return title;
    }

    public boolean isAllDay() {
        return allDay;
    }

    public long getStartMillis() {
        return startMillis;
    }

    public long getEndMillis() {
        return endMillis;
    }

    public Calendar getStartDate() {
        return startDate;
    }

    public Calendar getEndDate() {
        return endDate;
    }

    public String getTimeFormatted() {
        DateFormat dateFormat;
        DateFormat timeFormat;
        String formatted = null;
        if (allDay) {
            dateFormat = android.text.format.DateFormat.getDateFormat(context);
            formatted = dateFormat.format(startMillis);
        } else {
            dateFormat = android.text.format.DateFormat.getDateFormat(context);
            timeFormat = android.text.format.DateFormat.getTimeFormat(context);
            formatted = dateFormat.format(startMillis) + " " + timeFormat.format(startMillis) +
                    " - " + dateFormat.format(endMillis) + " " + timeFormat.format(endMillis);
        }
        return formatted;
    }

    public String getLocation() {
        return location;
    }

    public int getCalendarColor() {
        return calendarColor;
    }

    public CalendarEvent(Context context, int eventId, String title, boolean allDay, long startMillis, long endMillis, String location, int calendarColor) {
        this.context = context;
        this.eventId = eventId;
        this.title = title;
        this.allDay = allDay;
        this.startMillis = startMillis;
        this.endMillis = endMillis;
        this.location = location;
        this.calendarColor = calendarColor;
        this.startDate = Calendar.getInstance(Locale.getDefault());
        this.endDate = Calendar.getInstance(Locale.getDefault());

        startDate.setTimeInMillis(startMillis);
        endDate.setTimeInMillis(endMillis);
    }

    @Override
    public String toString() {
        return title + "\n" + (location != null ? (location + "\n") : "") + getTimeFormatted() + "\n";
    }
}
